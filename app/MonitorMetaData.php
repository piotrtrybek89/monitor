<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class MonitorMetaData extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'monitors_meta_data';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['total_time', 'redirect_count'];

    /**
     * Get the monitor that owns the meta data.
     */
    public function monitor()
    {
        return $this->belongsTo(Monitor::class);
    }

    /**
     * @param string $format
     * @return mixed
     */
    public function getCreatedAtFormatted(string $format = 'Y-m-d H:i:s')
    {
        return $this->created_at->format($format);
    }
}
