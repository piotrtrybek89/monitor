<?php
namespace App\Services\Monitor;

use App\Monitor;
use Illuminate\Http\Response;

class MonitorService implements MonitorServiceInterface
{
    /** @var string $httpCode */
    private $httpCode = '';

    /** @var int $redirectCount */
    private $redirectCount = 0;

    /** @var float $totalTime */
    private $totalTime = 0.0;

    /** @var Monitor $monitor */
    private $monitor;

    /**
     * @param Monitor $monitor
     * @return $this
     */
    public function process(Monitor $monitor): self
    {
        $this->monitor = $monitor;
        $this->fetchInfoAboutUrl();

        return $this;
    }

    /**
     * @return bool
     */
    public function isOK(): bool
    {
        return Response::HTTP_OK === (int) $this->httpCode;
    }

    /**
     * @return string
     */
    public function getHttpCode(): string
    {
        return $this->httpCode;
    }

    /**
     * @return int
     */
    public function getRedirectCount(): int
    {
        return $this->redirectCount;
    }

    /**
     * @return float
     */
    public function getTotalTime(): float
    {
        return $this->totalTime;
    }

    /**
     * @return array
     */
    protected function fetchInfoAboutUrl()
    {
        $ch = curl_init($this->monitor->url);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, (int) config('app.curl_timeout'));
        curl_exec($ch);

        if (!curl_errno($ch)) {
            $info = curl_getinfo($ch);
            $this->setHttpCode($info['http_code']);
            $this->setRedirectCount($info['redirect_count']);
            $this->setTotalTime($info['total_time']);
        }

        curl_close($ch);

        return null;
    }

    /**
     * @param string $httpCode
     * @return $this
     */
    private function setHttpCode(string $httpCode): self
    {
        $this->httpCode = $httpCode;

        return $this;
    }

    /**
     * @param int $redirectCount
     * @return $this
     */
    private function setRedirectCount(int $redirectCount): self
    {
        $this->redirectCount = $redirectCount;

        return $this;
    }

    /**
     * @param string $totalTime
     * @return $this
     */
    private function setTotalTime(string $totalTime): self
    {
        $this->totalTime = $totalTime;

        return $this;
    }
}
