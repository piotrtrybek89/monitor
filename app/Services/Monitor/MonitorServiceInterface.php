<?php
namespace App\Services\Monitor;

use App\Monitor;

interface MonitorServiceInterface
{
    /**
     * @param Monitor $monitor
     * @return mixed
     */
    public function process(Monitor $monitor);

    /**
     * @return bool
     */
    public function isOK(): bool;

    /**
     * @return string
     */
    public function getHttpCode(): string;

    /**
     * @return int
     */
    public function getRedirectCount(): int;

    /**
     * @return float
     */
    public function getTotalTime(): float;
}
