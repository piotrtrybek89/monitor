<?php
namespace App;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Monitor extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'monitors';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['url', 'queued_at'];

    /**
     * Get the meta data for the monitor.
     */
    public function monitorMetaData()
    {
        return $this->hasMany(MonitorMetaData::class);
    }

    /**
     * @return bool
     */
    public function isQueued(): bool
    {
        return $this->queued_at !== null;
    }

    /**
     * @return Collection
     */
    public function getMonitorMetaDataFromLastTenMinutes(): Collection
    {
        return $this->monitorMetaData()
           ->where('created_at', '>=', DB::raw('NOW() - INTERVAL 10 MINUTE'))
           ->orderBy('created_at', 'DESC')
           ->get();
    }

    /**
     * @param int $limit
     * @return Collection
     */
    public function getMonitorMetaDataLatest(int $limit = 1): Collection
    {
        return $this->monitorMetaData()->orderBy('created_at', 'DESC')->limit($limit)->get();
    }
}
