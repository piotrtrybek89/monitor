<?php
namespace App\Providers;

use App\Repositories\MonitorRepository;
use App\Repositories\MonitorRepositoryInterface;
use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->bind(
            MonitorRepositoryInterface::class,
            MonitorRepository::class
        );
    }
}
