<?php
namespace App\Jobs;

use App\Monitor;
use App\MonitorMetaData;
use App\Services\Monitor\MonitorService;
use App\Services\Monitor\MonitorServiceInterface;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class MonitorJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Delete the job if its models no longer exist.
     *
     * @var bool
     */
    public $deleteWhenMissingModels = true;

    /** @var Monitor $monitor */
    protected $monitor;

    /**
     * MonitorJob constructor.
     * @param Monitor $monitor
     */
    public function __construct(Monitor $monitor)
    {
        $this->monitor = $monitor;
    }

    /**
     * @param MonitorServiceInterface $monitorService
     */
    public function handle(MonitorServiceInterface $monitorService)
    {
        /** @var MonitorService $result */
        $result = $monitorService->process($this->monitor);

        if ($result->isOK()) {
            $monitorMetaData = new MonitorMetaData([
                'redirect_count' => $result->getRedirectCount(),
                'total_time' => $result->getTotalTime(),
            ]);

            $this->monitor->monitorMetaData()->save($monitorMetaData);
        }
    }
}
