<?php
namespace App\Console\Commands;

use App\Jobs\MonitorJob;
use App\Repositories\MonitorRepository;
use Illuminate\Console\Command;

class PushMonitorJob extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'monitor:push';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Push the monitor job to the queue';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @param MonitorRepository $monitorRepo
     */
    public function handle(MonitorRepository $monitorRepo)
    {
        $i = 0;
        $limit = 500;

        $monitors = $monitorRepo->getToQueue($limit, $i * $limit);
        while(!$monitors->isEmpty()) {
            $monitors->each(function ($monitor) {
                MonitorJob::dispatch($monitor);
            });
            $i++;
            $monitors = $monitorRepo->getToQueue($limit, $i * $limit);
        }
    }
}
