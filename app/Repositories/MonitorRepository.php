<?php
namespace App\Repositories;

use App\Monitor;
use Illuminate\Database\Eloquent\Collection;

class MonitorRepository implements MonitorRepositoryInterface
{
    /**
     * @inheritDoc
     */
    public function insertMany(array $urls)
    {
        foreach ($urls as $url) {
            $monitor = Monitor::firstOrNew(['url' => $url]);
            $monitor->save();
        }
    }

    /**
     * @inheritDoc
     */
    public function getManyByUrls(array $urls)
    {
        return Monitor::whereIn('url', $urls)->get();
    }

    /**
     * @inheritDoc
     */
    public function getOneByUrl(string $url): ?Monitor
    {
        return Monitor::where('url', $url)->first();
    }

    /**
     * @inheritDoc
     */
    public function getNotQueued(int $limit = null): Collection
    {
        $monitor = Monitor::whereNull('queued_at');

        if(!empty($limit)) {
            $monitor->limit($limit);
        }

        return $monitor->get();
    }

    /**
     * @inheritDoc
     */
    public function getToQueue(int $limit = 10, int $offset = 0): Collection
    {
        return Monitor::orderBy('id', 'ASC')->offset($offset)->limit($limit)->get();
    }
}
