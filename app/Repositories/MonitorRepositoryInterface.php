<?php
namespace App\Repositories;

use App\Monitor;
use Illuminate\Database\Eloquent\Collection;

interface MonitorRepositoryInterface
{
    /**
     * @param array $urls
     * @return mixed
     */
    public function insertMany(array $urls);

    /**
     * @param array $urls
     * @return mixed
     */
    public function getManyByUrls(array $urls);

    /**
     * @param string $url
     * @return Monitor
     */
    public function getOneByUrl(string $url): ?Monitor;

    /**'
     * @param int|null $limit
     * @return Collection
     */
    public function getNotQueued(int $limit = null): Collection;

    /**
     * @param int $limit
     * @param int $offset
     * @return Collection
     */
    public function getToQueue(int $limit = 10, int $offset = 0): Collection;
}
