<?php
namespace App\Http\Controllers;

use App\Http\Controllers\Interfaces\MonitorInterface;
use App\Http\Requests\GetMonitorsRequest;
use App\Http\Requests\PostMonitorsRequest;
use App\Http\Resources\MonitorMetaData;
use App\Http\Resources\MonitorWithStats;
use App\Repositories\MonitorRepositoryInterface;
use App\Services\Monitor\MonitorServiceInterface;
use Illuminate\Http\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class MonitorController extends Controller implements MonitorInterface
{
    /** @var MonitorRepositoryInterface $monitorRepo */
    protected $monitorRepo;

    /** @var MonitorServiceInterface $monitorService */
    protected $monitorService;

    /**
     * MonitorController constructor.
     * @param MonitorRepositoryInterface $monitorRepo
     * @param MonitorServiceInterface $monitorService
     */
    public function __construct(MonitorRepositoryInterface $monitorRepo, MonitorServiceInterface $monitorService)
    {
        $this->monitorRepo = $monitorRepo;
        $this->monitorService = $monitorService;
    }

    /**
     * @param PostMonitorsRequest $request
     * @return JsonResponse
     */
    public function postMonitors(PostMonitorsRequest $request): JsonResponse
    {
        $this->monitorRepo->insertMany($request->post('urls'));

        if (!$request->get('stats')) {

            return response()->json([
                'code' => Response::HTTP_OK,
                'data' => [],
                'message' => 'success',
            ]);
        }

        $monitors = $this->monitorRepo->getManyByUrls($request->get('urls'));

        return response()->json([
            'code' => Response::HTTP_OK,
            'data' => [],
            'message' => 'success',
        ])->header('X-Stats', json_encode(new MonitorWithStats($monitors)));
    }

    /**
     * @param GetMonitorsRequest $request
     * @param string $url
     * @return JsonResponse
     */
    public function getMonitors(GetMonitorsRequest $request, string $url): JsonResponse
    {
        $monitor = $this->monitorRepo->getOneByUrl($url);

        if (empty($monitor)) {
            return response()->json([
                'code' => Response::HTTP_NOT_FOUND,
                'data' => [],
                'errors' => [
                    'Model not found.'
                ],
            ])->setStatusCode(Response::HTTP_NOT_FOUND);
        }

        return response()->json([
            'code' => Response::HTTP_OK,
            'data' => MonitorMetaData::collection($monitor->getMonitorMetaDataFromLastTenMinutes()),
            'message' => 'success',
        ]);
    }
}
