<?php
namespace App\Http\Controllers\Interfaces;

use App\Http\Requests\GetMonitorsRequest;
use App\Http\Requests\PostMonitorsRequest;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

interface MonitorInterface
{
    /**
     * @param PostMonitorsRequest $request
     * @return JsonResponse
     */
    public function postMonitors(PostMonitorsRequest $request): JsonResponse;

    /**
     * @param GetMonitorsRequest $request
     * @param string $url
     * @return JsonResponse
     */
    public function getMonitors(GetMonitorsRequest $request, string $url): JsonResponse;
}
