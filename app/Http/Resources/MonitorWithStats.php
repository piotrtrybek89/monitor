<?php
namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class MonitorWithStats extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $result = [];
        $this->each(function ($item) use (&$result) {
            $result[$item->url] = $item->getMonitorMetaDataLatest()->first()->total_time ?? null;
        });

        return $result;
    }
}
