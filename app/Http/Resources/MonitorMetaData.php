<?php
namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class MonitorMetaData extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'redirectCount' => $this->redirect_count,
            'totalTime' => $this->total_time,
            'createdAt' =>$this->getCreatedAtFormatted(),
        ];
    }
}
