<?php
namespace App\Http\Requests;

class PostMonitorsRequest extends BaseFormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'stats' => 'sometimes|boolean',
            'urls' => 'required|array',
            'urls.*' => 'url|distinct'
        ];
    }

    /**
     *  Filters to be applied to the input.
     *
     * @return array
     */
    public function filters()
    {
        return [
            'urls.*' => 'trim|lowercase',
        ];
    }

    /**
     * @inheritDoc
     */
    public function authorize()
    {
        return true;
    }
}
