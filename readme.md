# The Monitor Project

## Necessary services:
- PHP 7.1+ (with the curl extension)
- Laravel 5.8
- Mysql 5.7 or MariaDB 10.2
- Redis 3.0+
- Supervisor


## Installation:

- ```composer install```
- copy .env.example to .env (if this file doesn't exist) and set the correct values
- ```php artisan migrate```
- copy the ```laravel-worker.conf``` to the ```/etc/supervisor/conf.d``` 
- ```sudo supervisorctl reread```
- ```sudo supervisorctl update```
- ```sudo supervisorctl start laravel-worker:*```
- add the task to crontab ```* * * * * /usr/bin/php /home/piotr/Projects/monitor/artisan schedule:run >> /var/log/schedule.log 2>&1```
- ```php artisan serve```


## API:
```
POST http://localhost:8000/monitors

Headers:
Accept:application/json
Content-Type:application/json

Body:
{
	"urls": [
		"https://onet.pl",
		"http://socialmention.com/",
		"http://test-redirects.137.software",
		"https://google.com"
	]
}

With the optional parameter (return the additional header 'X-Stats'):
POST http://localhost:8000/monitors?stats=1
```

```
GET http://localhost:8000/monitors/https%3A%2F%2Fonet.pl
```
